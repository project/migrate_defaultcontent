<?php

/**
 * Class DefaultContentNodeMigration
 * A class common to all DefaultContent migrations of entity type 'Node'
 */
class DefaultContentNodeMigration extends DrupalNode7Migration {
  public function __construct(array $arguments) {
    // Override map with UUID
    $map = $this->map = new MigrateSQLMap($arguments['machine_name'],
      array(
        'uuid' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Source UUID',
          'alias' => 'uunid',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );
    parent::__construct($arguments);
    if (isset($map)) {
      $this->map = $map;
    }
    $this->source = new MigrateSourcePHP($arguments['source_file']);
    if ($this->source->computeCount() == 0) {
      return;
    }

    $common = new MigrateDefaultContentCommon();
    // Assume a 1:1 mapping of fieldnames.
    $common->setFieldMapping($this);

    // @TODO revisions and node->uid / user references
    // @TODO find some way to document unmapped pseudo-fields
    // We don't map subfields, but rather pass the arguments in prepareRow().
    // We only need to do this explicitly for 'body', since migrate_d2d adds these itself
    $this->removeFieldMapping('body:summary');
    $this->removeFieldMapping('body:format');
    // Do not map the nid from source to the destination, they shouldn't need to be the same
    $this->removeFieldMapping('nid');
    $this->removeFieldMapping('vid');
  }

  /**
   * @param $row
   *
   * @return bool|void
   */
  public function prepareRow($row) {
    // Check destination database for existing nodes by uuid.
    if (empty($row->migrate_map_destid1)) {
      $existing_nid = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->condition('uuid', $row->uuid)
        ->execute()
        ->fetchField();
    }
    elseif ($row->migrate_map_needs_update == MigrateMap::STATUS_IGNORED &&
      $row->migrate_map_rollback_action == MigrateMap::ROLLBACK_PRESERVE) {
      $existing_nid = $row->migrate_map_destid1;
    }

    if (!empty($existing_nid)) {
      // Matched nodes are the source content and will not be rolled back.
      $hash = isset($row->migrate_map_hash) ? $row->migrate_map_hash : NULL;
      $this->map->saveIDMapping($row, array($existing_nid), MigrateMap::STATUS_IGNORED,
        MigrateMap::ROLLBACK_PRESERVE, $hash);
      $this->rollbackAction = MigrateMap::ROLLBACK_PRESERVE;
      return FALSE;
    }

    $common = new MigrateDefaultContentCommon();

    // Remove the path array if pathauto = 1 to prevent double url aliases for entity
    if (isset($row->path) && isset($row->pathauto) && $row->pathauto) {
      unset($row->path);
    }

    // Make sure poll nodes have a 'votes' property since Migrate requires one.
    if ($row->type == 'poll' && !isset($row->votes)) {
      $row->votes = array();
    }

    $common->prepareFields($row, $this);
  }

  /**
   * @param $entity_id
   */
  public function prepareRollback($entity_id) {
    $common = new MigrateDefaultContentCommon();
    $common->rollbackEntity($entity_id, 'node');
 }

  /**
   * @param $entity
   * @param stdClass $source_row
   */
  public function complete($entity, stdClass $source_row)  {
    $common = new MigrateDefaultContentCommon();
    $common->completeEntity($entity, 'node');
  }
}
