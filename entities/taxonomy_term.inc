<?php

/**
 * Class DefaultContentTermMigration
 */
class DefaultContentTermMigration extends DrupalTerm7Migration {
  public function __construct(array $arguments) {
    // Override map with UUID
    $map = $this->map = new MigrateSQLMap($arguments['machine_name'],
      array(
        'uuid' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Source UUID',
          'alias' => 'uutid',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );
    parent::__construct($arguments);
    if (isset($map)) {
      $this->map = $map;
    }
    $this->source = new MigrateSourcePHP($arguments['source_file']);
    if ($this->source->computeCount() == 0) {
      return;
    }
    $common = new MigrateDefaultContentCommon();
    // Assume a 1:1 mapping of fieldnames.
    $common->setFieldMapping($this);

    // Do not map the tid from source to the destination, they shouldn't need to be the same
    $this->removeFieldMapping('tid');
    $this->removeFieldMapping('vid');
  }

  /**
   * @param $row
   *
   * @return bool|void
   */
  public function prepareRow($row) {
    // Check destination database for existing taxonomy terms by uuid.
    if (empty($row->migrate_map_destid1)) {
      $existing_tid = db_select('taxonomy_term_data', 't')
        ->fields('t', array('tid'))
        ->condition('uuid', $row->uuid)
        ->execute()
        ->fetchField();
    }
    elseif ($row->migrate_map_needs_update == MigrateMap::STATUS_IGNORED &&
      $row->migrate_map_rollback_action == MigrateMap::ROLLBACK_PRESERVE) {
      $existing_tid = $row->migrate_map_destid1;
    }

    if (!empty($existing_tid)) {
      // Matched taxonomy is the source content and will not be rolled back.
      $hash = isset($row->migrate_map_hash) ? $row->migrate_map_hash : NULL;
      $this->map->saveIDMapping($row, array($existing_tid), MigrateMap::STATUS_IGNORED,
        MigrateMap::ROLLBACK_PRESERVE, $hash);
      $this->rollbackAction = MigrateMap::ROLLBACK_PRESERVE;
      return FALSE;
    }

    $common = new MigrateDefaultContentCommon();
    $common->prepareFields($row, $this);
  }

  /**
   * @param $entity_id
   */
  public function prepareRollback($entity_id) {
    $common = new MigrateDefaultContentCommon();
    $common->rollbackEntity($entity_id, 'taxonomy_term');
  }

  /**
   * @param $entity
   * @param stdClass $source_row
   */
  public function complete($entity, stdClass $source_row)  {
    $common = new MigrateDefaultContentCommon();
    $common->completeEntity($entity, 'taxonomy_term');
  }
}
