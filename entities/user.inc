<?php

/**
 * Class DefaultContentUserMigration
 * A class common to all DefaultContent migrations of entity type 'User'
 */
class DefaultContentUserMigration extends DrupalUser7Migration {
  public function __construct(array $arguments) {
    // Override map with UUID
    $map = $this->map = new MigrateSQLMap($arguments['machine_name'],
      array(
        'uuid' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Source UUID',
          'alias' => 'uuuid',
        ),
      ),
      MigrateDestinationUser::getKeySchema()
    );
    parent::__construct($arguments);
    if (isset($map)) {
      $this->map = $map;
    }
    $this->source = new MigrateSourcePHP($arguments['source_file']);
    if ($this->source->computeCount() == 0) {
      return;
    }

    $common = new MigrateDefaultContentCommon();
    // Assume a 1:1 mapping of fieldnames.
    $common->setFieldMapping($this);

    // @TODO user->uid / user references
    // @TODO find some way to document unmapped pseudo-fields
    // Do not map the uid from source to the destination, they shouldn't need to be the same.
    $this->removeFieldMapping('uid');
  }

  /**
   * @param $row
   *
   * @return bool|void
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Check destination database for existing users by uuid.
    if (empty($row->migrate_map_destid1)) {
      $existing_uid = db_select('users', 'u')
        ->fields('u', array('uid'))
        ->condition('uuid', $row->uuid)
        ->execute()
        ->fetchField();
    }
    elseif ($row->migrate_map_needs_update == MigrateMap::STATUS_IGNORED &&
      $row->migrate_map_rollback_action == MigrateMap::ROLLBACK_PRESERVE) {
      $existing_uid = $row->migrate_map_destid1;
    }

    if (!empty($existing_uid)) {
      // Matched users are the source content and will not be rolled back.
      $hash = isset($row->migrate_map_hash) ? $row->migrate_map_hash : NULL;
      $this->map->saveIDMapping($row, array($existing_uid), MigrateMap::STATUS_IGNORED,
        MigrateMap::ROLLBACK_PRESERVE, $hash);
      $this->rollbackAction = MigrateMap::ROLLBACK_PRESERVE;
      return FALSE;
    }

    $common = new MigrateDefaultContentCommon();

    // Map the user's roles. For this to work, import roles via features or by
    // hand, before running migrate.
    $results = array();
    if (!empty($row->roles)) {
      $role_names = array_values($row->roles);
      $query = db_select('role', 'r')
        ->fields('r', array('rid', 'name'))
        ->condition('r.name', $role_names, 'IN');
      $results = $query->execute();
    }

    // Add the authenticated role. All users have this role.
    $roles = array(DRUPAL_AUTHENTICATED_RID => DRUPAL_AUTHENTICATED_RID);
    foreach ($results as $result_row) {
      $roles[$result_row->rid] = $result_row->rid;
    }
    $row->roles = $roles;

    // Remove the path array if pathauto = 1 to prevent double url aliases for entity
    if (isset($row->path) && isset($row->pathauto) && $row->pathauto) {
      unset($row->path);
    }

    $common->prepareFields($row, $this);
  }

  /**
   * @param $entity_id
   */
  public function prepareRollback($entity_id) {
    $common = new MigrateDefaultContentCommon();
    $common->rollbackEntity($entity_id, 'user');
 }

  /**
   * @param $entity
   * @param stdClass $source_row
   */
  public function complete($entity, $source_row)  {
    $common = new MigrateDefaultContentCommon();
    $common->completeEntity($entity, 'user');
  }
}
