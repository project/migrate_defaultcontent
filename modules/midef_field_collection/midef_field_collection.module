<?php
/**
 * @file
 * Hook implementations for the midef_field_collection module.
 */

/**
 * Implements hook_content_export_export_alter().
 */
function midef_field_collection_content_export_export_alter($entity, $type) {
  if ($type == 'node') {
    $field_info = field_info_field_map();
    // Find field_collection-type fields and replace references with UUIDs.
    foreach ($entity as $fieldname => $field) {
      if (isset($field_info[$fieldname]) && $field_info[$fieldname]['type'] == 'field_collection') {
        foreach ($field as $language => $instance) {
          foreach ($instance as $delta => $values) {
            $uuid = entity_get_uuid_by_id('field_collection_item', array($values['value']));
            // Add UUIDs to array.
            $entity->{$fieldname}[$language][$delta]['value'] = current($uuid);
          }
        }
      }
    }
  }
}

/**
 * Implements hook_migrate_defaultcontent_register_migration().
 */
function midef_field_collection_migrate_defaultcontent_register_migration() {
  return array(
    array(
      'name' => 'field_collection_item',
      'class' => 'DefaultContentFieldCollectionMigration',
    ),
  );
}

/**
 * Implements hook_migrate_defaultcontent_register_fieldtype().
 */
function midef_field_collection_migrate_defaultcontent_register_fieldtype() {
  return array('field_collection');
}

/**
 * Implements hook_migrate_defaultcontent_prepare_fieldtype().
 */
function midef_field_collection_migrate_defaultcontent_prepare_fieldtype($migration, &$field, $field_type, $field_name, $row) {
  $common = new MigrateDefaultContentCommon();
  if ($field_type == 'field_collection') {
    // Attempts to resolve a reference to a field_collection that's already been
    // imported.
    // Skip revision ID, set it after resolving references.
    foreach ($field as $language => $instance) {
      foreach ($instance as $delta => $values) {
        unset($field[$language][$delta]['revision_id']);
      }
    }
    $common->prepareReferenceRow($field, $field_name, 'value', 'field_collection_item', $migration, $row);
    // Add revision ID to arguments.
    if (!empty($field)) {
      foreach ($field as $delta => $value) {
        if (is_numeric($delta)) {
          $result = db_query('SELECT MAX(revision_id)
            FROM {field_collection_item_revision} WHERE item_id = :item_id',
            array(':item_id' => $value));
          $field['arguments']['revision_id'][$delta] = $result->fetchField();
        }
      }
    }
  }
}

/**
 * Implements hook_migrate_defaultcontent_reference_alter().
 */
function midef_field_collection_migrate_defaultcontent_reference_alter($re_wrapper, $entity_wrapper, $reference) {
  $host_id = $re_wrapper->getIdentifier();
  $id = $entity_wrapper->getIdentifier();
  $result = db_query('SELECT MAX(revision_id)
            FROM {field_collection_item_revision} WHERE item_id = :item_id',
    array(':item_id' => $id));
  // Set revision ID.
  $host = $re_wrapper->value();
  // @TODO don't assume LANGUAGE_NONE,
  // would need language to be passed in $reference.
  $host->{$reference['field_name']}[LANGUAGE_NONE][$reference['field_delta']]['revision_id'] = $result->fetchField();
  field_attach_update($re_wrapper->type(), $host);
  entity_get_controller($re_wrapper->type())->resetCache(array($host_id));
}
