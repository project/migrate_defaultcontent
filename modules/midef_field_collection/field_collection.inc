<?php
/**
 * Class DefaultContentFieldCollectionDestination.
 *
 * The same as MigrateDestinationFieldCollection, but without a "host entity"
 * requirement.
 */
class DefaultContentFieldCollectionDestination extends MigrateDestinationFieldCollection {
  function __construct($bundle) {
    // Call parent constructor with unused dummy "host".
   parent::__construct($bundle, array('host_entity_type' => 'node'));
  }

  public function fields() {
    $fields = parent::fields();
    unset($fields['host_entity_id']);
  }

  public function import(stdClass $collection, stdClass $row) {
    if (isset($row->migrate_map_destid1)) {
      // We're updated an existing entity - start from the previous data.
      // entity_load() returns an array, so we get the field collection entity
      // with array_shift().
      $entity = array_shift(entity_load('field_collection_item', array($row->migrate_map_destid1), array(), TRUE));
      $entity_old = clone $entity;
      $updating = TRUE;
    }
    else {
      // Set a dummy hostEntityId so that we can bypass hardcoded validation.
      $entity = entity_create('field_collection_item', array('field_name' => $this->bundle, 'hostEntityId' => 1));
      $updating = FALSE;
    }

    foreach ((array) $collection as $field => $value) {
      $entity->{$field} = $value;
    }

    $this->prepare($entity, $row);

    // Restore fields from original field_collection_item if updating.
    if ($updating) {
      foreach ($entity as $field => $value) {
        if ('field_' != substr($field, 0, 6)) {
          continue;
        }
        elseif (property_exists($entity_old, $field) && !property_exists($collection, $field)) {
          $entity->$field = $entity_old->$field;
        }
      }
    }

    migrate_instrument_start('field_collection_save');
    // Set a fake host entity,
    // ensure this isn't saved anywhere by setting $skip_host_save=TRUE
    $status = $entity->save(TRUE);
    migrate_instrument_stop('field_collection_save');

    if ($status !== FALSE) {
      $this->complete($entity, $row);
      if ($updating) {
        $this->numUpdated++;
      }
      else {
        $this->numCreated++;
      }
      return array($entity->item_id);
    }
    else {
      return FALSE;
    }
  }
}

/**
 * Class DefaultContentFieldCollectionMigration
 */
class DefaultContentFieldCollectionMigration extends Migration {
  public function __construct(array $arguments) {
    // Override map with UUID
    $this->destination = new DefaultContentFieldCollectionDestination($arguments['destination_type']);
    $map = $this->map = new MigrateSQLMap($arguments['machine_name'],
      array(
        'uuid' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'Source UUID',
          'alias' => 'uufcid',
        ),
      ),
      MigrateDestinationFieldCollection::getKeySchema()
    );
    parent::__construct($arguments);
    if (isset($map)) {
      $this->map = $map;
    }
    $this->source = new MigrateSourcePHP($arguments['source_file']);
    if ($this->source->computeCount() == 0) {
      return;
    }

    $common = new MigrateDefaultContentCommon();
    // Assume a 1:1 mapping of fieldnames.
    $common->setFieldMapping($this);

    // Do not map the item_id from source to the destination,
    // they shouldn't need to be the same.
    $this->removeFieldMapping('item_id');
  }

  /**
   * @param array $row
   *
   * @return bool|void
   */
  public function prepareRow($row) {
    $common = new MigrateDefaultContentCommon();
    $common->prepareFields($row, $this);
    // Attempts to resolve a reference to a node that's already been imported.
    // Otherwise, it will save the reference as 'unresolved' and check for it,
    // until it finds an entity with that UUID.
    //$common->prepareReferenceRow($field, $field_name, $key, $target_type, $migration, $row)
    // @TODO record / attempt mapping to host entity
  }

  /**
   * @param $entity_id
   */
  public function prepareRollback($entity_id) {
    $common = new MigrateDefaultContentCommon();
    $common->rollbackEntity($entity_id, 'field_collection_item');
  }

  /**
   * @param $entity
   * @param stdClass $source_row
   */
  public function complete($entity, stdClass $source_row)  {
    $common = new MigrateDefaultContentCommon();
    $common->completeEntity($entity, 'field_collection_item');
  }
}
