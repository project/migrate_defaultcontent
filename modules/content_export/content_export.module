<?php
/**
 * @file
 * Code for the content_export submodule.
 */

/**
 * Implements hook_module_implements_alter().
 */
function content_export_module_implements_alter(&$implementations, $hook) {
  // Push our entity_load to the bottom of the list
  if ($hook == 'entity_load') {
    $group = $implementations['content_export'];
    unset($implementations['content_export']);
    $implementations['content_export'] = $group;
  }
}


/**
 * Implements hook_entity_load().
 */
function content_export_entity_load($entities, $type) {
  // Check for get param and access.
  if (isset($_GET['export']) && user_access('access devel information')) {
    static $progress;
    require_once 'includes/utility.inc';
    if (empty($_GET['export']) || $type == $_GET['export']) {

      // Invoke type specific load hooks since these are invoked after
      // hook_entity_load by default.
      $bundles = array();
      foreach ($entities as $key => $original_entity) {
        list(, , $bundle) = entity_extract_ids($type, $original_entity);
        $bundles[$bundle] = $bundle;
      }
      module_invoke_all($type . '_load', $entities, array_keys($bundles));

      foreach ($entities as $key => $original_entity) {
        if (!isset($progress[$type][$key])) {
          $progress[$type][$key] = TRUE;
          $exportable = content_export_generate_export($original_entity, $type);
          $info = entity_get_info($type);
          if (!empty($info['uuid'])) {
            $uuid_key = $info['entity keys']['uuid'];
            if (isset($exportable->{$uuid_key})) {
              $key = $exportable->{$uuid_key};
            }
          }
          $export[$key] = $exportable;
        }
      }
      if (!empty($export)) {
        print '<div><b>' . $type . '</b><br/><textarea cols="120" rows="20">' . check_plain(drupal_var_export($export)) . '</textarea></div>';
      }
    }
  }
}

/**
 * Helper function: Generate a re-usable php object to be used by var_export.
 *
 * @param $original_entity
 * @param $type
 *
 * @return object
 * @internal param $entity
 */
function content_export_generate_export($original_entity, $type) {
  $entity = clone $original_entity;
  // Entity exports generate exports using JSON,
  // which causes mixups between arrays and objects.
  // Simply generate an export of the publically available properties of the
  // object and cast it to a stdClass.
  $class = get_class($entity);
  if (is_object($entity) && $class !== 'stdClass') {
    $vars = get_object_vars($entity);
    unset($vars['is_new']);
    $entity = (object) $vars;
  }

  // Alterations for specific entity types.
  switch ($type) {
    case 'node':
      // Add pathauto status for this entity.
      if (module_exists('pathauto')) {
        $entity->pathauto = $entity->path['pathauto'];
      }

      // Attach path to the node.  Drupal doesn't attach this anymore for
      // performance reasons http://drupal.org/node/332333#comment-2163634.
      $entity->path = path_load(array('source' => 'node/' . $entity->nid));

      // Fix menu array.
      $entity->menu = content_export_get_node_menu($entity);
      break;

    case 'taxonomy_term':
      content_export_add_term_parent($entity);
      break;
  }

  content_export_replace_entity_ids($entity);
  if (module_exists('role_field')) {
    content_export_replace_role_ids($entity);
  }
  // Invokes hook_content_export_export_alter().
  drupal_alter('content_export_export', $entity, $type);
  return $entity;
}

/**
 * Our copy of node_export_get_menu().
 */
function content_export_get_node_menu($node) {
  // This will fetch the existing menu item if the node had one.
  module_invoke_all('node_prepare', $node);

  // Don't export anything here if there's no link_title.
  if (empty($node->menu['link_title'])) {
    unset($node->menu);
    return;
  }

  $type = $node->type;

  // Only keep the values we care about.
  // Store a copy of the old menu.
  $old_menu = $node->menu;

  // Now fetch the defaults for a new menu entry.
  $node = new stdClass();
  $node->type = $type;
  node_object_prepare($node);

  // Make a list of values to attempt to copy.
  $menu_fields = array(
    'link_title',
    'plid',
    'menu_name',
    'weight',
    'hidden',
    'expanded',
    'has_children',
  );

  // Copy those fields from the old menu over the new menu defaults.
  foreach ($menu_fields as $menu_field) {
    $node->menu[$menu_field] = $old_menu[$menu_field];
  }

  // Set a menu parent identifier.
  if (!empty($node->menu['plid'])) {
    $parent_item = menu_link_load($node->menu['plid']);
    $node->menu['parent_identifier'] = migrate_defaultcontent_generate_menu_link_id($parent_item);
  }

  // Copy the menu description from the old menu.
  // Issue #1287300.
  if (isset($old_menu['options']['attributes']['title'])) {
    $node->menu['description'] = $old_menu['options']['attributes']['title'];
  }
  else {
    $node->menu['description'] = '';
  }

  // Ensure menu will be created during node import.
  // Issue #1139120.
  $node->menu['enabled'] = 1;

  // Return the menu.
  return $node->menu;
}

/**
 * Replace entity reference IDs (nids, tids etc) with UUIDs.
 * @param $entity
 */
function content_export_replace_entity_ids($entity) {
  $field_info = field_info_field_map();

  // Define the field types to replace.
  $replace = array('taxonomy_term_reference', 'entityreference');

  // Loop trough the fields for an entity and replace the defined types.
  foreach ($entity as $field_name => $field) {
    if (isset($field_info[$field_name])) {
      if (in_array($field_info[$field_name]['type'], $replace)) {
        // Dynamically determine entity type referenced.
        if ($field_info[$field_name]['type'] == 'taxonomy_term_reference') {
          $entity_type = 'taxonomy_term';
          $target_key = 'tid';
        }
        elseif ($field_info[$field_name]['type'] == 'entityreference') {
          // Fetch additional field info to determine entity type referenced.
          $full_field_info = field_info_field($field_name);
          $entity_type = $full_field_info['settings']['target_type'];
          $target_key = 'target_id';
        }

        foreach ($field as $language => $values) {
          foreach ($values as $delta => $value) {
            // Replace referenced entity ID with UUID.
            $referenced_uuid = entity_get_uuid_by_id($entity_type, array($value[$target_key]));
            if (!empty($referenced_uuid)) {
              $entity->{$field_name}[$language][$delta][$target_key] = reset($referenced_uuid);
            }
          }
        }
      }
    }
  }
}

/**
 * Add taxonomy term parents to the entity export.
 */
function content_export_add_term_parent($entity) {
  $parents = taxonomy_get_parents($entity->tid);
  $entity->term_parent_uuid = NULL;
  if (!empty($parents)) {
    $entity->term_parent_uuid = current($parents)->uuid;
  }
}

/**
 * Replaces role ids with role names.
 *
 * @param $entity
 */
function content_export_replace_role_ids($entity) {
  $field_info = field_info_field_map();

  static $user_roles;
  if (!isset($user_roles)) {
    $user_roles = user_roles();
  }

  // Replace each role id with its associated role name.
  foreach ($entity as $field_name => $field) {
    if (!empty($field_info[$field_name]) && $field_info[$field_name]['type'] === 'role') {
      foreach ($field as $language => $values) {
        foreach ($values as $delta => $value) {
          if (isset($user_roles[$value['value']])) {
            $entity->{$field_name}[$language][$delta]['value'] = $user_roles[$value['value']];
          }
        }
      }
    }
  }
}
