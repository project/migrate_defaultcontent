# Meta
* Drupal version: 7.x
* Module version: 7.x-1.0-alpha4
* Dependencies: Migrate (2.6), Migrate D2D
* Devs: Freek van Rijt

# Introduction
The migrate default-content is a proposed module to tackle the issue of having certain content (entities) like
taxonomy terms and nodes available when installing a Drupal site. It takes into account the fact that default content
may need to be managed post-launch or post-install, where clients will be changing content on a production or staging environment.

The module is intended for developers or site owners.

Examples are certain taxonomy structures which are needed for overviews, certain nodes which are needed to make the
site appear as it should, or to add demo / placeholder content which the client can use as a starting point.

# Why Migrate?
The migrate module is a highly flexible module that allows for content from a given source
(be it json, xml or another database) to be mapped and then migrated into Drupal.

Features like creating placeholders "stubs" for referenced entities as well as detecting new- or changed content in
the source(s) used are suitable for building a "default content" based on the migrate module.

The goal of the migrate default-content module then, is to allow for a good
DX in managing content between site instances and across installations, without the limitations of node_export,
deploy and features. The idea is to have a module that is fully dynamic: the developer only needs to adhere to a
certain file-naming pattern for his exports (eg. entity-bundle.inc) - the module would then handle the rest: map the
source fields 1:1 with the destination (as we can assume the config is stored in features or is otherwise the same on
the source and destination environments). The module should support the most common field types, references and allow
for file imports (eg. image or file fields on nodes). Migrations should be able to be managed via migrate-UI and
existing content should only be overridden with exports on-demand.

# Usage
1. To export entities, use the included content_export module. Appending ?export on any page displaying one or more entities will
return a list of text fields with export code.

2. Add a folder sites/all/defaultcontent. Use the included content export module to export one or more entities of the same bundle.
The naming scheme for detection is entity-bundle.inc. For example, adding a node-article.inc will add a migration of
nodes of the bundle 'article'.

3. These files should return an array of node objects to import. See the examples in the /examples folder for this module.

4. If your entities have files attached to them, place these files in sites/all/defaultcontent/files and they will be
imported to the file directory along with your entity.

5. To detect newly added export files, clear the Drupal cache.

6. To import, run drush migration commands or use the migrate UI module. Using drush commands you could automatically
run migrations to update all the content on deployment or post-commit.

If UUID is installed, the module will use UUIDs as identifiers in exports as well as map the desinationID to the UUID. This
provides the best workflow when working on content as a team and prevents entity-ID conflicts when exporting.

# Limitations
There is no reliable way to handle node-ID conflicts between seperate environments when exporting. However, when migrating,
content IDs themselves will not be transferred over - they will simply be used to cross reference entities, and keep
track of which nodes have been migrated. Thus, if you export for example a node which has an ID already contained
in one of the export files, you can simply change this ID in the export. Make sure to also change any exports referencing to
that ID as well.

There is currently no class available to import field collections by referring to them from a node. Instead, field collections
themselves must contain a host_entity ID in their export. This, however, limits these collections to only one node.
