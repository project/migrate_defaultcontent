<?php
/**
 * @file
 * Admin forms for migrate_defaultcontent.
 */
function migrate_defaultcontent_admin_form($form, $form_state) {
  $form['migrate_defaultcontent_scan_directory'] = array(
    '#title' => t('Default content directory'),
    '#type' => 'textfield',
    '#default_value' => variable_get('migrate_defaultcontent_scan_directory', MIGRATE_DEFAULTCONTENT_SCAN_DIRECTORY),
    '#description' => t('Define the directory to scan, relative to the drupal root')
  );

  $form['#submit'][] = 'migrate_defaultcontent_admin_form_submit';

  return system_settings_form($form);
}

/**
 * Submit handler for migrate_defaultcontent_admin_form().
 */
function migrate_defaultcontent_admin_form_submit($form, &$form_state) {
  migrate_defaultcontent_register_migrations();
  $form_state['redirect'] = 'admin/content/migrate';
}
