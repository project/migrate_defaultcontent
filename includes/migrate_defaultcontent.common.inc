<?php
/**
 * @file
 * Class DefaultContentCommon
 * Default mapping actions to take for all types of entities.
 */
class MigrateDefaultContentCommon {

  /**
   * Define the field mapping from source to destination
   * @param $migration
   */
  public function setFieldMapping($migration) {
    foreach ($migration->getSource()->fields() as $fieldname => $description) {
      // Term-parent, which is an invention of content_export will add
      // taxonomy terms in their right tree-position.
      // @TODO look-up this fields value using the new reference methodology.
      if ($fieldname == 'term_parent_uuid') {
        $migration->addFieldMapping('parent', 'term_parent_uuid', FALSE)
          ->sourceMigration($migration->getMachineName());
      }
      elseif ($fieldname == 'uid') {
        // For now, assign all nodes to user 1.
        // @TODO In the future, with user migrations supported, refactor this.
        $migration->addFieldMapping('uid', 'uid', FALSE)
          ->defaultValue(1);
      }
      // Fetch information on the source fields in the destination DB.
      $field_info = field_info_field($fieldname);
      $type = $field_info['type'];

      // Only map fields of types that we know and understand how to import
      // Allow other modules to add support for other field types.
      $field_types = module_invoke_all('migrate_defaultcontent_register_fieldtype');
      if (in_array($type, $field_types) || empty($field_info)) {
        // Do not use addSimpleMappings because we'd get spammed with
        // 'override' messages in the UI.
        $migration->addFieldMapping($fieldname, $fieldname, FALSE);
      }
      else {
        $migration->addFieldMapping(NULL, $fieldname, FALSE)
          ->issueGroup(t('Unsupported'));
      }

      // Allow other modules to do something after a 1-1 mapping has been made.
      module_invoke_all('migrate_defaultcontent_field_mapping', $migration);
    }
  }

  /**
   * Massage fields on a per-type basis.
   *
   * Ensures that they are understood by the field destination class.
   *
   * We don't use field handlers as these would prevent other migrations
   * outside of Default Content from working right.
   */
  public function prepareFields($row, $migration) {
    foreach ($row as $field_name => &$field) {
      if (isset($field)) {
        $field_info = field_info_field($field_name);
        if (empty($field_info)) {
          // Handle core fields.
          switch ($field_name) {
            case 'path':
              $this->preparePathRow($field);
              break;

            case 'menu':
              $this->prepareMenuLinkRow($field);
              break;

            case 'uid':
              // Fallback to default (1)
              $field = NULL;
          }
          continue;
        }
        $type = $field_info['type'];
        // These field-types are processed by the MigrateValueFieldHandler.
        $value_fields = array(
          'value',
          'list',
          'list_boolean',
          'list_integer',
          'list_float',
          'list_text',
          'number_integer',
          'number_decimal',
          'number_float',
          'machine_name',
        );
        switch ($type) {
          // Handle text fields
          // @see MigrateTextFieldHandler.
          case 'text':
          case 'text_long':
          case 'text_with_summary':
          case 'date':
          case 'datestamp':
          case 'datetime':
            $this->prepareValueArrayRow($field);
            break;

          case 'viewfield':
            $this->prepareValueArrayRow($field, 'vname');
            break;

          case 'blockreference':
            $this->prepareValueArrayRow($field, 'moddelta');
            break;

          case 'registration':
            $this->prepareValueArrayRow($field, 'registration_type');
            break;

          case 'addressfield':
            $this->prepareValueArrayRow($field, 'country');
            break;

          case 'geofield':
            $this->prepareValueArrayRow($field, 'geom');
            break;

          case 'link_field':
            $this->prepareValueArrayRow($field, 'url');
            break;

          case 'email':
            $this->prepareValueArrayRow($field, 'email');
            break;

          case in_array($type, $value_fields):
            $this->prepareValueRow($field);
            break;

          // Handle file- and image fields.
          case 'image':
          case 'file':
            $this->prepareFileRow($field);
            break;

          case 'taxonomy_term_reference':
          case 'entityreference':
            if ($type == 'taxonomy_term_reference') {
              $target_type = 'taxonomy_term';
              $key = 'tid';
            }
            else {
              $target_type = $field_info['settings']['target_type'];
              $key = 'target_id';
            }
            $this->prepareReferenceRow($field, $field_name, $key, $target_type, $migration, $row);
            break;

          case 'role':
            $this->prepareRoleRow($field);
            break;
        }
        // Allow other modules to add prepare functions for other field types.
        foreach (module_implements('migrate_defaultcontent_prepare_fieldtype') as $module) {
          $function = $module . '_migrate_defaultcontent_prepare_fieldtype';
          $function($migration, $field, $type, $field_name, $row);
        }
      }
    }
  }

  /**
   * Prepare text row for processing by Migrate.
   *
   * @param array $field
   *   The $field array passed from the mgirate source.
   */
  public function prepareValueArrayRow(&$field, $value_key = 'value') {
    $temp_field = $field;
    $field = array();
    foreach ($temp_field as $lang => $body) {
      if (is_array($temp_field[$lang])) {
        foreach ($temp_field[$lang] as $delta => $instance) {
          foreach ((array) $instance as $key => $value) {
            $args[$key][$delta] = $value;
          }
          $field[$delta] = $instance[$value_key];
          $field['arguments'] = isset($args) ? $args : array();
          // @TODO test with entity_translation
          $field['arguments']['language'] = $lang;
        }
      }
    }
    unset($temp_field);
  }

  /**
   * Prepare a simple value row for processing by Migrate
   *
   * @param $field
   *
   * @internal param $row
   */
  public function prepareValueRow(&$field, $value_key = 'value') {
    $temp_field = $field;
    $field = array();
    foreach ($temp_field as $lang => $body) {
      if (is_array($temp_field[$lang])) {
        foreach ($temp_field[$lang] as $delta => $instance) {
          $field[$delta] = $instance[$value_key];
          $field['arguments'] = array();
          // @TODO test with entity_translation
          $field['arguments']['language'] = $lang;
        }
      }
    }
    unset($temp_field);
  }

  /**
   * Prepare a simple value row for processing by Migrate
   *
   * @param $field
   *
   * @internal param $row
   */
  public function prepareFileRow(&$field) {
    $temp_field = $field;
    $field = array();
    foreach ($temp_field as $lang => $body) {
      if (is_array($temp_field[$lang])) {
        foreach ($temp_field[$lang] as $delta => $instance) {
          foreach ((array)$instance as $key => $value) {
            $args[$key][$delta] = $value;
          }
          // $instance['filename'] doesn't contain dupe-entry suffix (eg filename_0.jpg) which we need if we want to be
          // able to copy the file directly from the files dir.
          $field[$delta] = drupal_basename($instance['uri']);
          $directory = variable_get('defaultcontent_scan_directory', MIGRATE_DEFAULTCONTENT_SCAN_DIRECTORY);
          $field['arguments'] = isset($args) ? $args : array();
          $field['arguments']['source_dir'] = $directory . '/files';
          // @TODO test with entity_translation
          $field['arguments']['language'] = $lang;
          $field['arguments']['file_replace'] = FILE_EXISTS_REPLACE;
        }
      }
    }
    unset($temp_field);
  }

  /**
   * Prepare a field with references to other entities.
   *
   * @param array $field
   *   The field array as passed from the source migration.
   * @param string $field_name
   *   The field name / array key from the source migration.
   * @param string $key
   *   The array key in the source migration which contains the ID-value.
   * @param string $target_type
   *   The type of entity this is a reference to.
   * @param object $migration
   *   The migration class for the currently active migration.
   * @param array $row
   *   The current migration row.
   */
  public function prepareReferenceRow(&$field, $field_name, $key, $target_type, $migration, $row) {
    $temp_field = $field;
    $field = array();
    if (!empty($temp_field)) {
      $arguments = $migration->getArguments();
      foreach ($temp_field as $lang => $items) {
        if (is_array($items)) {
          foreach ($items as $delta => $item) {
            // Look up the UUID directly in the destination DB.
            $entity_id = entity_get_id_by_uuid($target_type, array($item[$key]));
            if (!empty($entity_id)) {
              $field[$delta] = reset($entity_id);
              $field['arguments']['language'] = $lang;
              if ($key == 'tid') {
                $field['arguments']['source_type'] = $key;
              }
            }
            else {
              $class = get_class($migration);
              switch ($class) {
                case 'DefaultContentNodeMigration':
                  $source_entity = 'node';
                  break;

                case 'DefaultContentTermMigration':
                  $source_entity = 'taxonomy_term';
                  break;
              }
              // Look up the reference whenever a new entity is migrated.
              $source_key = key($migration->getMap()->getSourceKey());
              $reference = array(
                'target_id' => $item[$key],
                'source_uuid' => $row->{$source_key},
                'bundle' => $arguments['source_type'],
                'entity_type' => $source_entity,
                'field_name' => $field_name,
                'field_delta' => $delta,
                // The UUID for the referencing entity will be set in the
                // migrations complete method.
              );
              $existing_references = variable_get('defaultcontent_references', array());
              // Store unresolved referenced to be retreived in
              // later migrations.
              $existing_references[$reference['target_id']][] = $reference;
              variable_set('defaultcontent_references', $existing_references);
            }
          }
        }
      }
    }
    if (empty($field)) {
      $field = NULL;
    }
  }

  /**
   * Path migrations apparently do not support languages yet, but they should take the language of the host entity
   * @param $field
   */
  public function preparePathRow(&$field) {
    $field = $field['alias'];
  }

  /**
   * Prepare a menu link for importing
   * @param $field
   */
  public function prepareMenuLinkRow(&$field) {
    // Ensure we don't override an existing link by accident
    unset($field['mlid']);
    // Attempt to find the parent, if the export contains a "features-style" parent identifier
    if (isset($field['parent_identifier'])) {
      // Look up identifier in menu_links
      $parent = migrate_defaultcontent_lookup_menu_link($field['parent_identifier']);

      // Set parent link ID
      if ($parent) {
        $field['plid'] = $parent['mlid'];
      }
    }
  }

  /**
   * Replaces the role name with its associated role id.
   *
   * @param $field
   */
  public function prepareRoleRow(&$field) {
    static $user_roles;
    if (!isset($user_roles)) {
      $user_roles = user_roles();
    }

    $temp_field = $field;
    $field = array();
    foreach ($temp_field as $lang => $body) {
      if (is_array($temp_field[$lang])) {
        foreach ($temp_field[$lang] as $delta => $instance) {
          foreach ((array)$instance as $key => $value) {
            $role_id = array_search($value, $user_roles);
            if ($role_id !== FALSE) {
              $value = $role_id;
            }
            $args[$key][$delta] = $value;
          }
          $field[$delta] = (($role_id = array_search($instance['value'], $user_roles)) !== FALSE) ? $role_id : $instance['value'];
          $field['arguments'] = isset($args) ? $args : array();
          // @TODO test with entity_translation
          $field['arguments']['language'] = $lang;
        }
      }
    }
    unset($temp_field);
  }

  /**
   * Common actions for entity rollbacks
   * @param $entity_id
   * @param $entity_type
   */
  public function rollbackEntity($entity_id, $entity_type) {
    if (!is_array($entity_id)) {
      $entity_id = array($entity_id);
    }
    // @TODO this doesn't work for entities that are not nodes
    $entity_uuid = entity_get_uuid_by_id($entity_type, $entity_id);
    // Remove references that don't need resolving anymore
    $existing_references = variable_get('defaultcontent_references', array());
    foreach ($existing_references as $target_id => $references) {
      foreach ($references as $key => $reference) {
        if ($reference['source_uuid'] == current($entity_uuid)) {
          unset($existing_references[$target_id][$key]);
        }
      }
      if (empty($existing_references[$target_id])) {
        unset($existing_references[$target_id]);
      }
    }
    variable_set('defaultcontent_references', $existing_references);
  }

  /**
   * Code to execute after an entity has been added to Drupal.
   *
   * @param $entity
   * @param $type
   */
  public function completeEntity($entity, $type) {
    // Store unresolved referenced, to be retreived in later migrations
    $existing_references = variable_get('defaultcontent_references', array());
    if (!empty($existing_references)) {
      // Attempt to resolve references
      $this->resolveReferences($entity, $type, $existing_references);
    }
  }


  /**
   * Given an entity and an array of unresolved references, add references
   * to that entity to previously migrated objects.
   *
   * @param $entity
   *  The fully migrated entity object
   * @param $source_type
   * @param $references
   *  The array of references to check
   */
  private function resolveReferences($entity, $source_type, $references) {
    // Fetch the UUID key to use on the current entity
    $info = entity_get_info($source_type);
    // Find out what entity keys to use.
    $uuid_key = $info['entity keys']['uuid'];
    // If the references array contains the UUID of the entity we just created,
    // add the newly added ID of this entity to those unresolved reference fields
    if (isset($references[$entity->{$uuid_key}])) {
      foreach ($references[$entity->{$uuid_key}] as $reference) {
        // Entity_uuid_load changes all references to normal IDs back to uuid,
        // so we need to fetch the entity using the normal entity_load instead
        $ids = entity_get_id_by_uuid($reference['entity_type'], array($reference['source_uuid']));
        // Load the entities that contain the unresolved reference
        $referencing_entity = reset(entity_load($reference['entity_type'], $ids));
        if (empty($referencing_entity)) {
          return;
        }
        // Create an entitywrapper for the entity we just imported
        $entity_wrapper = entity_metadata_wrapper($source_type, $entity);
        // Create an entitywrapper for the entity we want to update
        $re_wrapper = entity_metadata_wrapper($reference['entity_type'], $referencing_entity);
        $id = $entity_wrapper->getIdentifier();
        // If no field-delta is provided, assume a non-fieldAPI field that has a simple value
        if (!isset($reference['field_delta']) || $re_wrapper->{$reference['field_name']} instanceof EntityDrupalWrapper) {
          $re_wrapper->{$reference['field_name']} = intval($id);
        }
        else {
          $re_wrapper->{$reference['field_name']}[$reference['field_delta']] = intval($id);
        }
        drupal_alter('migrate_defaultcontent_reference', $re_wrapper, $entity_wrapper, $reference);
        $re_wrapper->save();
      }
      // Mark the reference as "resolved" by removing it from tracking
      unset($references[$entity->{$uuid_key}]);
      variable_set('defaultcontent_references', $references);
    }
  }
}
