<?php

/**
 * Implementation of MigrateSource, to handle imports from PHP exports
 */
class MigrateSourcePHP extends MigrateSource {
  protected $numRows;
  protected $currentFileContents;
  protected $url;

  /**
   * @param array $url
   * @param array $options
   */
  public function __construct($url, array $options = array()) {
    parent::__construct($options);
    $this->url = $url;
    $this->currentFileContents = include($url);
    if (!is_array($this->currentFileContents) || empty($this->currentFileContents)) {
      Migration::displayMessage(t('!url is not a PHP file containing an array of objects',
        array('!url' => $this->url)));
      return FALSE;
    }
    else {
      $this->numRows = count($this->currentFileContents);
    }
  }

  /**
   * Return a string representing the source, for display in the UI.
   */
  public function __toString() {
    return (string)$this->url;
  }

  /**
   * Returns a list of fields available to be mapped from the source,
   * keyed by field name.
   */
  public function fields() {
    $fields = NULL;
    if ($this->numRows) {
      // Use the last export in the file, this is likely to be the most recent and thus the most accurate
      $fields = get_object_vars(end($this->currentFileContents));
      foreach ($fields as $fieldname => &$value) {
        $value = ''; // @TODO fill this with field_info stuff
      }
    }
    return $fields;
  }

  /**
   * Return the number of available source records.
   */
  public function computeCount() {
    return isset($this->numRows) ? $this->numRows : 0;
  }

  /**
   * Do whatever needs to be done to start a fresh traversal of the source data.
   *
   * This is always called at the start of an import, so tasks such as opening
   * file handles, running queries, and so on should be performed here.
   */
  public function performRewind() {
    reset($this->currentFileContents);
  }

  /**
   * Fetch the next row of data, returning it as an object. Return FALSE
   * when there is no more data available.
   */
  public function getNextRow() {
    if ($row = current($this->currentFileContents)) {
      // Advance pointer
      next($this->currentFileContents);
      return $row;
    }
    return FALSE;
  }
}
