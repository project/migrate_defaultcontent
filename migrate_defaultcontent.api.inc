<?php
/**
 * @file
 * Hook documentation for hooks exposed by migrate defaultcontent.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Register a new entity-type for inclusion into defaultcontent.
 *
 * This is purely a metadata function, you'll need to write your own
 * class to handle the actual importing / handling of these entities.
 *
 * @return array
 *   An array containing metadata for registering a defaultcontent migration
 *   - name: The entity name, used in finding the export files and used for
 *     naming the migration in the UI. Eg. the name 'foo' will lookup
 *     foo-*bundle*.inc and name the migrations MDC_foo_*bundle&
 *   - class: The class name to use for the migration, the file this class is
 *     contained in, MUST be included in the .info file of your module.
 */
function hook_migrate_defaultcontent_register_migration() {
  return array(
    array(
      'name' => 'Foo',
      'class' => 'DefaultContentFooMigration',
    ),
  );
}

/**
 * Allows for the alteration of the default arguments passed to the migration
 * registration handler.
 *
 * @param $arguments
 *  An array containing the arguments to be used for a migration
 * @param $migration
 *  An array containing metadata as registered in hook_migrate_defaultcontent_register_migration().
 */
function hook_migrate_default_content_arguments_alter(&$arguments, $migration) {
  // Used source_vocabulary and destination_vocabulary instead of _type
  if ($migration['name'] == 'taxonomy_term') {
    $arguments['source_vocabulary'] = $arguments['source_type'];
    unset($arguments['source_type']);
    $arguments['destination_vocabulary'] = $arguments['destination_type'];
    unset($arguments['destination_type']);
  }
}

/**
 * Declare support for a field type.
 *
 * If a field type is not listed as supported, it will not be mapped. You
 * should also implement migrate_defaultcontent_prepare_fieldtype() to prepare
 * the field type for importing.
 *
 * @return array
 *  An array containing the machine names of the field type(s) your module offers
 *  support for.
 */
function hook_migrate_defaultcontent_register_fieldtype() {
  return array('foo_field');
}

/**
 * Prepare field-data in a migration row for importing.
 *
 * The most common use case to use this hook is if you want to implement support
 * for a field type that is not included with this module.
 *
 * @param $migration
 *  The migration object for the migration the current $row belongs to
 * @param $field
 *  Unprocessed $field data from a single migration row
 * @param $field_type
 *  The type of field, as retreived by field_get_info on the destination site
 */
function hook_migrate_defaultcontent_prepare_fieldtype($migration, &$field, $field_type) {
  if ($field_type == 'foo_field') {
    $field = $field['foo_value'];
  }
}

/**
 * Alter the metadata wrapper prior to saving resolved references. Call in
 * migrate's complete() function by entity classes.
 *
 * @param EntityMetadataWrapper $re_wrapper
 *  The MetadataWrapper for the entity whose refrences are being resolved
 * @param EntityMetadataWrapper $entity_wrapper
 *  The MetadataWrapper for the just-imported entity, referenced by a field in
 *  $re_wrapper
 * @param $reference
 *  An array containing information on the reference that's being resolved
 */
function hook_migrate_defaultcontent_reference_alter(EntityMetadataWrapper $re_wrapper, EntityMetadataWrapper $entity_wrapper, $reference) {
  // For my entity type called 'Foo', also set a reference to a "vid" field when
  // the "nid" field was just resolved.
  if ($re_wrapper->type() == 'Foo' && $reference['field_name'] == 'nid') {
    $node = $re_wrapper->nid->value();
    $re_wrapper->vid->set($node->vid);
  }
}

/**
 * @} End of "addtogroup hooks".
 */
