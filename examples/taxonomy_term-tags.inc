<?php
return array(
  (object) array(
    'tid' => '19',
    'vid' => '1',
    'name' => 'abc',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'vocabulary_machine_name' => 'tags',
  ),
  (object) array(
    'tid' => '20',
    'vid' => '1',
    'name' => 'def',
    'description' => NULL,
    'format' => NULL,
    'weight' => '0',
    'vocabulary_machine_name' => 'tags',
  ),
  (object) array(
    'tid' => '21',
    'vid' => '1',
    'name' => 'tags',
    'description' => 'a description for my tag',
    'format' => 'filtered_html',
    'weight' => '0',
    'vocabulary_machine_name' => 'tags',
    'field_term_image' => array(
      'und' => array(
        array(
          'fid' => '77',
          'uid' => '1',
          'filename' => 'sample-file.png',
          'uri' => 'public://sample-file.png',
          'filemime' => 'image/png',
          'filesize' => '9491',
          'status' => '1',
          'timestamp' => '1398847748',
          'rdf_mapping' => array(),
          'alt' => '',
          'title' => '',
          'width' => '1213',
          'height' => '420',
        ),
      ),
    ),
  ),
);
